#!/bin/bash

aws secretsmanager get-secret-value --secret-id test/env-test | jq .SecretString | jq -r | jq -r  'keys[] as $k | "\($k)=\(.[$k])"' >> /var/www/html/.env

composer install