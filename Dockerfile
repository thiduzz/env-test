FROM studydrive/base-php8.0-alpine:1.0

ARG NEWRELIC_API_KEY
ARG NEWRELIC_APP_NAME

USER root

RUN docker-php-ext-enable opcache redis \
    && docker-php-source delete \
    && rm -rf /tmp/*

#Install and Configure NewRelic
RUN if [[ -n "$NEWRELIC_API_KEY" ]] && [[ -n "$NEWRELIC_APP_NAME" ]] ; then \
        echo -e "\nnewrelic.appname = \"${NEWRELIC_APP_NAME}\"" >> $PHP_INI_DIR/conf.d/newrelic.ini ; \
        echo -e "\nextension = \"newrelic.so\" " >> $PHP_INI_DIR/conf.d/newrelic.ini ; \
        NR_INSTALL_USE_CP_NOT_LN=1 NR_INSTALL_SILENT=1 NR_INSTALL_KEY="${NEWRELIC_API_KEY}" /var/lib/newrelic/newrelic-install install ; \
    fi

COPY ./.docker/app/prod.conf /etc/nginx/conf.d/default.conf

COPY ./.docker/app/entrypoint.sh /entrypoints/app-entrypoint.sh

COPY . /var/www/html

RUN chown -R docker:www-data /var/www/html

USER docker