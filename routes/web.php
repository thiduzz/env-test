<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/env-test-service', function(){
    return response()->json(['data'=> 'Healthy!'],200);
});

Route::get('/environment', function () {
    return config('app.envTest');
});

Route::get('/environments', function () {
    return array_merge(config()->all(), $_ENV);
});
